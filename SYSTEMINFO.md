# Microsoft Azure Kubernetes Service

Vendor: Microsoft
Homepage: https://www.microsoft.com/en-us/

Product: Azure Kubernetes Service
Product Page: https://azure.microsoft.com/en-us/products/kubernetes-service

## Introduction
We classify Azure AKS into the Cloud domain as AKS is a managed container orchestration service that simplifies deploying, managing, and scaling containerized applications using Kubernetes.

## Why Integrate
The Azure AKS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AKS. With this adapter you have the ability to perform operations on items such as:

- Get Managed Clusters
- Create Managed Cluster
- Delete Managed Cluster
- Get Agent Pool
- Create Agent Pool
- Delete Agent Pool

## Additional Product Documentation
The [API documents for Azure AKS](https://learn.microsoft.com/en-us/rest/api/aks/?view=rest-aks-2024-02-01)