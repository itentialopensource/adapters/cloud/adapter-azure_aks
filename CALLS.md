## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Azure AKS. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Azure AKS.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Microsoft Azure Kubernetes Service. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">operationsList(callback)</td>
    <td style="padding:15px">Gets a list of compute operations.</td>
    <td style="padding:15px">{base_path}/{version}/providers/Microsoft.ContainerService/operations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersList(callback)</td>
    <td style="padding:15px">Gets a list of managed clusters in the specified subscription.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/providers/Microsoft.ContainerService/managedClusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersListByResourceGroup(resourceGroupName, callback)</td>
    <td style="padding:15px">Lists managed clusters in the specified subscription and resource group.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersDelete(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Deletes a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersGet(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Gets a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersUpdateTags(resourceGroupName, resourceName, parameters, callback)</td>
    <td style="padding:15px">Updates tags on a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersCreateOrUpdate(resourceGroupName, resourceName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersGetAccessProfile(resourceGroupName, resourceName, roleName, callback)</td>
    <td style="padding:15px">Gets an access profile of a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/accessProfiles/{pathv4}/listCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersListClusterAdminCredentials(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Gets cluster admin credential of a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/listClusterAdminCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersListClusterMonitoringUserCredentials(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Gets cluster monitoring user credential of a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/listClusterMonitoringUserCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersListClusterUserCredentials(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Gets cluster user credential of a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/listClusterUserCredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersResetAADProfile(resourceGroupName, resourceName, parameters, callback)</td>
    <td style="padding:15px">Reset AAD Profile of a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/resetAADProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersResetServicePrincipalProfile(resourceGroupName, resourceName, parameters, callback)</td>
    <td style="padding:15px">Reset Service Principal Profile of a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/resetServicePrincipalProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersRotateClusterCertificates(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Rotate certificates of a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/rotateClusterCertificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">managedClustersGetUpgradeProfile(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Gets upgrade profile for a managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/upgradeProfiles/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">agentPoolsList(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Gets a list of agent pools in the specified managed cluster.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/agentPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">agentPoolsDelete(resourceGroupName, resourceName, agentPoolName, callback)</td>
    <td style="padding:15px">Deletes an agent pool.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/agentPools/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">agentPoolsGet(resourceGroupName, resourceName, agentPoolName, callback)</td>
    <td style="padding:15px">Gets the agent pool.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/agentPools/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">agentPoolsCreateOrUpdate(resourceGroupName, resourceName, agentPoolName, parameters, callback)</td>
    <td style="padding:15px">Creates or updates an agent pool.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/agentPools/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">agentPoolsGetUpgradeProfile(resourceGroupName, resourceName, agentPoolName, callback)</td>
    <td style="padding:15px">Gets upgrade profile for an agent pool.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/agentPools/{pathv4}/upgradeProfiles/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">agentPoolsGetAvailableAgentPoolVersions(resourceGroupName, resourceName, callback)</td>
    <td style="padding:15px">Gets a list of supported versions for the specified agent pool.</td>
    <td style="padding:15px">{base_path}/{version}/subscriptions/{pathv1}/resourceGroups/{pathv2}/providers/Microsoft.ContainerService/managedClusters/{pathv3}/availableAgentPoolVersions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
