
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:26PM

See merge request itentialopensource/adapters/adapter-azure_aks!12

---

## 0.4.3 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-azure_aks!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:38PM

See merge request itentialopensource/adapters/adapter-azure_aks!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:50PM

See merge request itentialopensource/adapters/adapter-azure_aks!8

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!7

---

## 0.3.4 [03-28-2024]

* Changes made at 2024.03.28_13:18PM

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!6

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_14:16PM

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_15:33PM

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!4

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:46AM

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!3

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!2

---

## 0.2.0 [05-21-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!1

---

## 0.1.1 [07-24-2021]

- Initial Commit

See commit b999238

---
