
## 0.2.0 [05-21-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-azure_aks!1

---

## 0.1.1 [07-24-2021]

- Initial Commit

See commit b999238

---
