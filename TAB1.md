# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Azure_aks System. The API that was used to build the adapter for Azure_aks is usually available in the report directory of this adapter. The adapter utilizes the Azure_aks API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Azure AKS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AKS. With this adapter you have the ability to perform operations on items such as:

- Get Managed Clusters
- Create Managed Cluster
- Delete Managed Cluster
- Get Agent Pool
- Create Agent Pool
- Delete Agent Pool

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
